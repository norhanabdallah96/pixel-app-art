// Select color input
// Select size input
var height, width, color;  
// When size is submitted by the user, call makeGrid()

$ ('#sizePicker').submit(function(event){
  event.preventDefault();
  height = $('#inputHeight').val();
  width =  $('#inputWidth').val();
  makeGrid(height, width);
})

function makeGrid(h, w) {
  // Your code goes here!
  $('tr').remove();   

  for (var x =1; x <= h; x++){
    $('#pixelCanvas').append('<tr id= table' + x + '></tr>');
    for (var y =1; y <= w; y++){
         $('#table' + x).append('<td></td>');
    }
  }

  $('td').click(function addColor(){
       color = $('#colorPicker').val();
       
       if ($(this).attr('style')){
           $(this).removeAttr('style') 
      } else {
        $(this).attr('style', 'background-color:' + color); 
      }
  })
}

